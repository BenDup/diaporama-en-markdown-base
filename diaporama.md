## Exemple de diaporama

Ceci est exemple de diaporama. Cliquez sur la flèche de droite (en bas à droite) pour passer à la diapositive suivante. Vous pouvez aussi appuyer sur la touche espace ou sur la flèche droite de votre clavier.

---

Il est possible de le modifier en ouvrant le menu de gauche (via le bouton en bas à gauche), puis en cliquant sur "Modifier la présentation" dans l'onglet "Outils".

---

Le contenu du diaporama est rédigé en markdown. Un language textuel simple et facile à utiliser pour mettre en forme du texte.

Il est ainsi possible de mettre du texte en **gras** ou en *italique*. 

---

Chaque diapositive est séparée par trois tirets (`---`).

Il est aussi possible de faire des [liens](https://fr.wikipedia.org/), et d'insérer des images :

![](reveal.js/plugin/chalkboard/img/sponge.png)


---

Ainsi que des listes ...

- Un
- Deux
- Trois

---

Du code ...

```python
def hello_world():
    print("Hello World !")
```

---

Et même des maths !

$$ {F}_{A/B} = G \times \frac{M_A M_B}{d^2} $$